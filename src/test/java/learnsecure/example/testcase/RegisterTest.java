package learnsecure.example.testcase;

import bankapplication.BankApplication;
import bankapplication.api.AuthenticationResponse;
import bankapplication.api.RegisterRequest;
import bankapplication.controller.RegisterController;
import bankapplication.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.AssertionErrors;

import java.util.Optional;

@SpringBootTest(classes = BankApplication.class)
public class RegisterTest {
	@Autowired
	private RegisterController registerController;
	@Test
	void NameExist() {
		try {
			ResponseEntity<AuthenticationResponse> res1 = registerController.register(new RegisterRequest(
					"Giang Văn Dương",
					"123qweA@",
					"giang11@gmail.com",
					"086759883511"
			));
			ResponseEntity<AuthenticationResponse> res2 = registerController.register(new RegisterRequest(
					"Dương Văn Giang",
					"123qweA@",
					"giang12@gmail.com",
					"0186759883512"
			));
			AssertionErrors.assertTrue("Fail", false);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("true", true);
		}
	}
	@Test
	void EmailExist() {
		try {
			ResponseEntity<AuthenticationResponse> res1 = registerController.register(new RegisterRequest(
					"giang22",
					"123qweA@",
					"giang21@gmail.com",
					"086759883521"
			));
			ResponseEntity<AuthenticationResponse> res2 = registerController.register(new RegisterRequest(
					"giang23",
					"123qweA@",
					"giang21@gmail.com",
					"0186759883522"
			));
			AssertionErrors.assertTrue("Fail", false);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("true", true);
		}
	}
	@Test
	void PhoneNumberExist() {
		try {
			ResponseEntity<AuthenticationResponse> res1 = registerController.register(new RegisterRequest(
					"giang31",
					"123qweA@",
					"giang31@gmail.com",
					"0867598835"
			));
			ResponseEntity<AuthenticationResponse> res2 = registerController.register(new RegisterRequest(
					"giang32",
					"123qweA@7",
					"giang32@gmail.com",
					"0867598835"
			));
			AssertionErrors.assertTrue("Fail", false);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("true", true);
		}
	}
	@Test
	void Success() {
		try {
			ResponseEntity<AuthenticationResponse> res1 = registerController.register(new RegisterRequest(
					"giang41",
					"123qweA@",
					"giang41@gmail.com",
					"086759883541"
			));

			AssertionErrors.assertTrue("true", true);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("false", false);
		}
	}

}
