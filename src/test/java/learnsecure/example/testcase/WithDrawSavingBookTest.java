package learnsecure.example.testcase;

import bankapplication.BankApplication;
import bankapplication.api.CreateSavingBookRequest;
import bankapplication.api.WithDrawMoneySavingBookRequest;
import bankapplication.controller.CreateSavingBookController;
import bankapplication.controller.DepositAmountSavingBookController;
import bankapplication.controller.SavingBookController;
import bankapplication.domain.Balance;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.enumType.InterestPaymentType;
import bankapplication.exception.FailedCreateSavingBook;
import bankapplication.repo.BalanceRepo;
import bankapplication.repo.RoleRepo;
import bankapplication.repo.SavingBookRepo;
import bankapplication.repo.UserRepo;
import bankapplication.service.BankService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.AssertionErrors;

import java.math.BigDecimal;
import java.util.Optional;

@SpringBootTest(classes = BankApplication.class)
public class WithDrawSavingBookTest {
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private SavingBookRepo savingBookRepo;
	@Autowired
	private RoleRepo roleRepo;
	@Autowired
	private BalanceRepo balanceRepo;
	@Autowired
	private BankService bankService;
	@Autowired
	private SavingBookController savingBookController;
	@Autowired
	private CreateSavingBookController createSavingBookController;

	@Autowired
	private DepositAmountSavingBookController depositAmountSavingBookController;
	// tat toan so tiet kiem OnBalance khong ton tai
	@Test
	void SavingBookNotExist() {
		User user = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser = balanceRepo.findByUser_id(user.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnBalance,
							balanceUser.getBalance().subtract(new BigDecimal(10.00)),
							new Long(1)),
					user
			);
			ResponseEntity<SavingBook> savingBookDeposit = depositAmountSavingBookController.depositSavingBook(
					new WithDrawMoneySavingBookRequest(
						new Long(100),
							savingBook.getBody().getDepositAmount()
					),
					user
			);
			AssertionErrors.assertTrue("Fail", false);
		} catch (FailedCreateSavingBook failedCreateSavingBook) {
			AssertionErrors.assertTrue("Success", true);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("Success", true);
		}
	}
	// tat toan so tiet kiem OnBalance ton tai nhưng không thuộc user
	@Test
	void UseNotHaveSavingBook() {
		User user1 = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser1 = balanceRepo.findByUser_id(user1.getId());
		User user2 = userRepo.findByUsername("justmegiang1@gmail.com");
		Balance balanceUser2 = balanceRepo.findByUser_id(user2.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnBalance,
							balanceUser1.getBalance().subtract(new BigDecimal(10.00)),
							new Long(1)),
					user1
			);
			ResponseEntity<SavingBook> savingBookDeposit = depositAmountSavingBookController.depositSavingBook(
					new WithDrawMoneySavingBookRequest(
							savingBook.getBody().getId(),
							savingBook.getBody().getDepositAmount()
					),
					user2
			);
			AssertionErrors.assertTrue("Fail", false);
		} catch (FailedCreateSavingBook failedCreateSavingBook) {
			AssertionErrors.assertTrue("Success", true);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("Success", true);
		}
	}
	@Test
	void withDrawDepositOnBalanceGreaterThanTotal() {
		User user = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser = balanceRepo.findByUser_id(user.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnBalance,
							balanceUser.getBalance().subtract(new BigDecimal(10.00)),
							new Long(1)),
					user
			);
			ResponseEntity<SavingBook> savingBookDeposit = depositAmountSavingBookController.depositSavingBook(
					new WithDrawMoneySavingBookRequest(
							savingBook.getBody().getId(),
							savingBook.getBody().getDepositAmount().add(new BigDecimal(10.0))
					),
					user
			);
			AssertionErrors.assertTrue("Fail", false);
		} catch (FailedCreateSavingBook failedCreateSavingBook) {
			System.out.println(failedCreateSavingBook);
			AssertionErrors.assertTrue("Success", true);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("Success", true);
		}
	}
	// tat toan onbalance thanh cong
	@Test
	void PassCase() {
		User user = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser = balanceRepo.findByUser_id(user.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnBalance,
							balanceUser.getBalance().subtract(new BigDecimal(10.00)),
							new Long(1)),
					user
			);
			ResponseEntity<SavingBook> savingBookDeposit = depositAmountSavingBookController.depositSavingBook(
					new WithDrawMoneySavingBookRequest(
							savingBook.getBody().getId(),
							savingBook.getBody().getDepositAmount()
					),
					user
			);

			if (savingBookRepo.findById(savingBook.getBody().getId()).isPresent()) {
				AssertionErrors.assertTrue("fail", false);
			}
			AssertionErrors.assertTrue("pass", true);
		} catch (Exception exception) {
			AssertionErrors.assertTrue("fail", false);
		}
	}
}
