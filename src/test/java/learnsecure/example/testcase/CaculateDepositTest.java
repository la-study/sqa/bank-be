package learnsecure.example.testcase;

import bankapplication.BankApplication;
import bankapplication.api.CreateSavingBookRequest;
import bankapplication.controller.CreateSavingBookController;
import bankapplication.controller.SavingBookController;
import bankapplication.domain.Balance;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.enumType.InterestPaymentType;
import bankapplication.exception.FailedCreateSavingBook;
import bankapplication.repo.BalanceRepo;
import bankapplication.repo.RoleRepo;
import bankapplication.repo.SavingBookRepo;
import bankapplication.repo.UserRepo;
import bankapplication.schedule.CaculateDeposit;
import bankapplication.service.BankService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.AssertionErrors;

import java.math.BigDecimal;
import java.util.Optional;

@SpringBootTest(classes = BankApplication.class)
public class CaculateDepositTest {
	@Autowired
	private CaculateDeposit caculateDeposit;
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private SavingBookRepo savingBookRepo;
	@Autowired
	private RoleRepo roleRepo;
	@Autowired
	private BalanceRepo balanceRepo;
	@Autowired
	private BankService bankService;
	@Autowired
	private SavingBookController savingBookController;
	@Autowired
	private CreateSavingBookController createSavingBookController;
	@Test
	void NotUpToTimeDeposit() {
		User user = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser = balanceRepo.findByUser_id(user.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnBalance,
							balanceUser.getBalance().subtract(new BigDecimal(10.00)),
							new Long(1)),
					user
			);
			Long idSavingBook = savingBook.getBody().getId();
			Optional<SavingBook> savingBook1 = savingBookRepo.findById(idSavingBook);
			if(savingBook1.isPresent()) {
				AssertionErrors.assertTrue("saving book exist on db", true);
			}
			caculateDeposit.updateSavingBook();
			if(savingBookRepo.findById(savingBook.getBody().getId()).isPresent()) {
				AssertionErrors.assertTrue("true", true);
			}
			AssertionErrors.assertTrue("pass", true);
		} catch (FailedCreateSavingBook failedCreateSavingBook) {
			AssertionErrors.assertTrue("fail2", false);
		}
	}
	@Test
	void caculateDepositOnBalance() {
		User user = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser = balanceRepo.findByUser_id(user.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnBalance,
							balanceUser.getBalance().subtract(new BigDecimal(10.00)),
							new Long(5)),
					user
			);
			Long idSavingBook = savingBook.getBody().getId();
			Optional<SavingBook> savingBook1 = savingBookRepo.findById(idSavingBook);
			if(savingBook1.isPresent()) {
				AssertionErrors.assertTrue("saving book exist on db", true);
			}
			caculateDeposit.updateSavingBook();
			if(savingBookRepo.findById(savingBook.getBody().getId()).isPresent()) {
				AssertionErrors.assertTrue("fail1", false);
			}
			AssertionErrors.assertTrue("pass", true);
		} catch (FailedCreateSavingBook failedCreateSavingBook) {
			AssertionErrors.assertTrue("fail2", false);
		}
	}
	@Test
	void caculateDepositOnDepositAmount() {
		User user = userRepo.findByUsername("justmegiang@gmail.com");
		Balance balanceUser = balanceRepo.findByUser_id(user.getId());
		try {
			ResponseEntity<SavingBook> savingBook = createSavingBookController.createNewSavingBook(
					new CreateSavingBookRequest(
							"name",
							InterestPaymentType.OnDepositAmount,
							balanceUser.getBalance().subtract(new BigDecimal(10.00)),
							new Long(5)),
					user
			);
			Long idSavingBook = savingBook.getBody().getId();
			Optional<SavingBook> savingBook1 = savingBookRepo.findById(idSavingBook);
			if(savingBook1.isPresent()) {
				AssertionErrors.assertTrue("saving book exist on db", true);
			}
			caculateDeposit.updateSavingBook();
			if(!savingBookRepo.findById(savingBook.getBody().getId()).isPresent()) {
				AssertionErrors.assertTrue("fail1", false);
			}
			AssertionErrors.assertTrue("pass", true);
		} catch (FailedCreateSavingBook failedCreateSavingBook) {
			AssertionErrors.assertTrue("fail2", false);
		}
	}
}
