package bankapplication.schedule;

import bankapplication.domain.Balance;
import bankapplication.domain.InterestRate;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.enumType.InterestPaymentType;
import bankapplication.repo.BalanceRepo;
import bankapplication.repo.SavingBookRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CaculateDeposit {
	private final SavingBookRepo savingBookRepo;
	private final BalanceRepo balanceRepo;
	@Scheduled(fixedDelay = 1000 * 30) // Runs every day
	public void updateSavingBook() {
		System.out.println("run");
		List<SavingBook> savingBooks = savingBookRepo.findAll();
		savingBooks.forEach(savingBook -> {
			InterestRate interestRate = savingBook.getInterestRate();
			Double rate = interestRate.getRate();
			Double tenture = interestRate.getTerm();
			LocalDateTime startSavingBookDate = savingBook.getUpdatedAt();
			LocalDateTime currentDateTime = LocalDateTime.now();
			Period period = Period.between(currentDateTime.toLocalDate(), startSavingBookDate.toLocalDate());
			int months = period.getMonths();
			if(months >= tenture) {
				InterestPaymentType interestPaymentType = savingBook.getInterestPaymentType();

				if(interestPaymentType == InterestPaymentType.OnBalance) {
					User user = savingBook.getUser();
					Balance balance = balanceRepo.findByUser_id(user.getId());
					System.out.println(1);
					BigDecimal newBalance = balance.getBalance().add(
							savingBook.getDepositAmount().
									multiply(new BigDecimal(rate/100.0 + 1)));
					balance.setBalance(newBalance);
					balanceRepo.save(balance);
					savingBookRepo.delete(savingBook);
				}
				if(interestPaymentType == InterestPaymentType.OnDepositAmount) {
					System.out.println(2);
					System.out.println("total: "+rate/100.0 + 1);
					savingBook.setDepositAmount(savingBook.getDepositAmount().
							multiply(new BigDecimal(rate/100.0 + 1)));
					savingBookRepo.save(savingBook);
				}
			}
		});

	}
}
