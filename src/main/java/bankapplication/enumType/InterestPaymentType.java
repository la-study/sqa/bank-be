package bankapplication.enumType;

public enum InterestPaymentType {
	OnBalance,
	OnDepositAmount
}
