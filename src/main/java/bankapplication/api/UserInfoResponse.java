package bankapplication.api;

import bankapplication.domain.Balance;
import bankapplication.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoResponse  {
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	User user;
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	Balance balance;
}
