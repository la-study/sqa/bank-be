package bankapplication.api;

import bankapplication.enumType.InterestPaymentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateSavingBookRequest {
	private String savingBookName;
	private InterestPaymentType interestPaymentType;
	private BigDecimal depositAmount;

	private Long interestRateId;

}
