package bankapplication.api;

import bankapplication.exception.FailedAuthenticated;
import bankapplication.exception.FailedRegister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class RestExceptionHandler {
	@ExceptionHandler(FailedAuthenticated.class)
	public ResponseEntity<Object> handleFailedAuthenticated(FailedAuthenticated ex) {
		Map<String, Object> errorResponse = new HashMap<>();
		errorResponse.put("status", HttpStatus.UNAUTHORIZED.value());
		errorResponse.put("message", ex.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(FailedRegister.class)
	public ResponseEntity<Object> handleFailedAuthenticated(FailedRegister ex) {
		Map<String, Object> errorResponse = new HashMap<>();
		errorResponse.put("status", HttpStatus.BAD_REQUEST.value());
		errorResponse.put("message", ex.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}
}
