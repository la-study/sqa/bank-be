package bankapplication.repo;

import bankapplication.domain.InterestRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterestRateRepo extends JpaRepository<InterestRate, Long> {

}
