package bankapplication.repo;

import bankapplication.domain.Balance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BalanceRepo extends JpaRepository<Balance, Long> {
	Balance findByUser_id(Long id);
}
