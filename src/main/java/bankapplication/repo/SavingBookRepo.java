package bankapplication.repo;

import bankapplication.domain.Balance;
import bankapplication.domain.SavingBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SavingBookRepo extends JpaRepository<SavingBook, Long> {
	List<SavingBook> findByUser_id(Long id);
}
