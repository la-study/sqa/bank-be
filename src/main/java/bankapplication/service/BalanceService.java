package bankapplication.service;

import bankapplication.domain.Balance;
import bankapplication.domain.User;
import bankapplication.repo.BalanceRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BalanceService {
	private final BalanceRepo balanceRepo;
	public Balance findBalanceByUser(User user) {
		return balanceRepo.findByUser_id(user.getId());
	}
}
