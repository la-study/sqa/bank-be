package bankapplication.service;

import bankapplication.api.CreateSavingBookRequest;
import bankapplication.api.WithDrawMoneySavingBookRequest;
import bankapplication.domain.Balance;
import bankapplication.domain.InterestRate;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.enumType.InterestPaymentType;
import bankapplication.exception.FailedCreateSavingBook;
import bankapplication.exception.FailedWithDraw;
import bankapplication.repo.*;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BankService {
	private final UserRepo userRepo;
	private final SavingBookRepo savingBookRepo;
	private final RoleRepo roleRepo;
	private final BalanceRepo balanceRepo;

	private final InterestRateRepo interestRateRepo;

	public List<SavingBook> getSavingBook(Long id, User user) {
		List<SavingBook> savingBooks = getAllSavingBook(user);
		List<SavingBook> resultSavingBook = new ArrayList<>();
		for (int i = 0; i < savingBooks.size(); i++) {
			if (savingBooks.get(i).getId().equals(id) == true) {
				resultSavingBook.add(savingBooks.get(i));
				return resultSavingBook;
			}
		}
		return resultSavingBook;
	}

	public Balance deposit(User user, BigDecimal deposit) {
		Balance balance = balanceRepo.findByUser_id(user.getId());
		balance.setBalance(balance.getBalance().add(deposit));
		balanceRepo.save(balance);
		return balance;
	}

	public List<SavingBook> getAllSavingBook(User user) {
		return savingBookRepo.findByUser_id(user.getId());
	}

	public SavingBook createSavingBook(CreateSavingBookRequest createSavingBookRequest, User user) {
		Balance balance = balanceRepo.findByUser_id(user.getId());
		BigDecimal totalMoney = balance.getBalance();
		Optional<InterestRate> interestRate = interestRateRepo.findById(createSavingBookRequest.getInterestRateId());
		if (totalMoney.compareTo(createSavingBookRequest.getDepositAmount()) == -1)
			throw new FailedCreateSavingBook("Creating saving book failed");
		balance.setBalance(balance.getBalance().subtract(createSavingBookRequest.getDepositAmount()));
		balanceRepo.save(balance);
		InterestPaymentType interestPaymentType =
				createSavingBookRequest.getInterestPaymentType() == InterestPaymentType.OnBalance ?
						InterestPaymentType.OnBalance : InterestPaymentType.OnDepositAmount;
		SavingBook savingBook = new SavingBook(
				null,
				createSavingBookRequest.getSavingBookName(),
				user,
				interestPaymentType,
				createSavingBookRequest.getDepositAmount(),
				interestRate.get(),
				LocalDateTime.now(),
				LocalDateTime.now(),
				null
				);
		savingBook = savingBookRepo.save(savingBook);
		return savingBook;
	}

	public SavingBook withDrawMoney(WithDrawMoneySavingBookRequest withDrawMoneySavingBookRequest, User user) {
		Long savingBookId = withDrawMoneySavingBookRequest.getId();
		Optional<SavingBook> savingBookOptional = savingBookRepo.findById(savingBookId);
		if (savingBookOptional.isPresent() == false) throw new FailedWithDraw("Failed With Draw");
		SavingBook savingBook = savingBookOptional.get();
		if (savingBook.getUser().getId().compareTo(user.getId()) != 0) throw new FailedWithDraw("Failed With Draw");
		if (savingBook.getDepositAmount().compareTo(withDrawMoneySavingBookRequest.getDepositAmount()) == -1)
			throw new FailedWithDraw("Failed With Draw");
		savingBook.setDepositAmount(savingBook.getDepositAmount().subtract(withDrawMoneySavingBookRequest.getDepositAmount()));
		savingBookRepo.save(savingBook);
		Balance balance = balanceRepo.findByUser_id(user.getId());
		balance.setBalance(balance.getBalance().add(withDrawMoneySavingBookRequest.getDepositAmount()));
		balanceRepo.save(balance);
		return savingBook;
	}
}
