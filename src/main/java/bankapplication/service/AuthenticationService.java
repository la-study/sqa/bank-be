package bankapplication.service;

import bankapplication.api.AuthenticationRequest;
import bankapplication.api.AuthenticationResponse;
import bankapplication.api.RegisterRequest;
import bankapplication.config.JwtService;
import bankapplication.domain.Balance;
import bankapplication.domain.User;
import bankapplication.exception.FailedAuthenticated;
import bankapplication.exception.FailedRegister;
import bankapplication.repo.BalanceRepo;
import bankapplication.repo.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
	private final UserRepo userRepo;
	private final BalanceRepo balanceRepo;
	private final JwtService jwtService;

	private final AuthenticationManager authenticationManager;
	private Boolean isUserExist(String username) {
		User user = userRepo.findByUsername(username);
		if(user != null) return true;
		return false;
	}
	public AuthenticationResponse register(RegisterRequest request) {
		if(isUserExist(request.getEmail())) {
			throw new FailedRegister("Register failed");

		};
		User user = new User(null, request.getName(), request.getEmail(), request.getPassword(),request.getPhone_number(), request.getEmail(), new ArrayList<>());
		User storeUser = userRepo.save(user);
		Balance balance = new Balance(null, storeUser, new BigDecimal("0"));
		balanceRepo.save(balance);
		String jwtToken = jwtService.generateToken(user);

		return AuthenticationResponse.builder().token(jwtToken).build();
	}

	public String authenticate(AuthenticationRequest request) {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
			);
		} catch (AuthenticationException ex) {
			System.out.println(ex.toString());
			throw new FailedAuthenticated("Authentication Failed");
		}

		User user = userRepo.findByUsername(request.getUsername());
		if (user == null || !user.getPassword().equals(request.getPassword())) {
			throw new FailedAuthenticated("Authentication Failed");
		}

		String jwtToken = jwtService.generateToken(user);
		return jwtToken;
	}
}
