package bankapplication.service;

import bankapplication.domain.InterestRate;
import bankapplication.repo.InterestRateRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InterestRateService {
	private final InterestRateRepo interestRateRepo;
	public List<InterestRate> getAllInterestRate() {
		return interestRateRepo.findAll();
	}
	public InterestRate saveInterestRate(InterestRate interestRate) {
		return interestRateRepo.save(interestRate);
	}
}
