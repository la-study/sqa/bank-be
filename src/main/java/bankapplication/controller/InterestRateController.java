package bankapplication.controller;

import bankapplication.domain.InterestRate;
import bankapplication.service.InterestRateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class InterestRateController {
	private final InterestRateService interestRateService;

	@GetMapping("/api/interest_rates")
	public ResponseEntity<List<InterestRate>> ListInterestRate() {
		return ResponseEntity.ok(interestRateService.getAllInterestRate());
	}
}
