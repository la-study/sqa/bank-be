package bankapplication.controller;

import bankapplication.api.CreateSavingBookRequest;
import bankapplication.domain.Balance;
import bankapplication.domain.InterestRate;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.enumType.InterestPaymentType;
import bankapplication.exception.FailedCreateSavingBook;
import bankapplication.repo.*;
import bankapplication.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class CreateSavingBookController {
	private final BankService bankService;
	private final UserRepo userRepo;
	private final SavingBookRepo savingBookRepo;
	private final RoleRepo roleRepo;
	private final BalanceRepo balanceRepo;

	private final InterestRateRepo interestRateRepo;
	@PostMapping("/api/savingbook")
	public ResponseEntity<SavingBook> createNewSavingBook(@RequestBody CreateSavingBookRequest createSavingBookRequest, @AuthenticationPrincipal User user) {
		Balance balance = balanceRepo.findByUser_id(user.getId());
		BigDecimal totalMoney = balance.getBalance();
		Optional<InterestRate> interestRate = interestRateRepo.findById(createSavingBookRequest.getInterestRateId());
		if (totalMoney.compareTo(createSavingBookRequest.getDepositAmount()) == -1)
			throw new FailedCreateSavingBook("Creating saving book failed");
		balance.setBalance(balance.getBalance().subtract(createSavingBookRequest.getDepositAmount()));
		balanceRepo.save(balance);
		InterestPaymentType interestPaymentType = InterestPaymentType.OnBalance;
		if(createSavingBookRequest.getInterestPaymentType() == InterestPaymentType.OnBalance) {
			interestPaymentType = InterestPaymentType.OnBalance;
		}
		else {
			interestPaymentType = InterestPaymentType.OnDepositAmount;
		}
		SavingBook savingBook = new SavingBook(
				null,
				createSavingBookRequest.getSavingBookName(),
				user,
				interestPaymentType,
				createSavingBookRequest.getDepositAmount(),
				interestRate.get(),
				LocalDateTime.now(),
				LocalDateTime.now(),
				null
		);
		savingBook = savingBookRepo.save(savingBook);
		return ResponseEntity.ok().body(
				savingBook
		);
	}
}
