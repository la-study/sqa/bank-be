package bankapplication.controller;

import bankapplication.api.AuthenticationResponse;
import bankapplication.api.RegisterRequest;
import bankapplication.config.JwtService;
import bankapplication.domain.Balance;
import bankapplication.domain.User;
import bankapplication.exception.FailedRegister;
import bankapplication.repo.BalanceRepo;
import bankapplication.repo.UserRepo;
import bankapplication.service.AuthenticationService;
import bankapplication.service.BalanceService;
import bankapplication.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
@RestController
@RequiredArgsConstructor
public class RegisterController {
	private final UserRepo userRepo;
	private final BalanceRepo balanceRepo;
	private final JwtService jwtService;

	private final AuthenticationManager authenticationManager;
	public Boolean isUserExist(String username) {
		User user = userRepo.findByUsername(username);
		if(user != null) return true;
		return false;
	}
	public Boolean isNameExist(String name) {
		return false;
	}
	public Boolean isPhoneNumberExist(String phoneNumber) {
		return false;
	}

	@PostMapping("/auth/register")
	public ResponseEntity<AuthenticationResponse> register(
			@RequestBody RegisterRequest request
	){
		if(isUserExist(request.getEmail())) {
			throw new FailedRegister("Register failed");
		};
		if(isPhoneNumberExist(request.getPhone_number())) {
			throw new FailedRegister("Register failed");
		}
		if(isNameExist(request.getName())) {
			throw new FailedRegister("Register failed");
		}
		User user = new User(null, request.getName(), request.getEmail(), request.getPassword(),request.getPhone_number(), request.getEmail(), new ArrayList<>());
		User storeUser = userRepo.save(user);
		Balance balance = new Balance(null, storeUser, new BigDecimal("0"));
		balanceRepo.save(balance);
		String jwtToken = jwtService.generateToken(user);

		return ResponseEntity.ok(AuthenticationResponse.builder().token(jwtToken).build());
	}
}
