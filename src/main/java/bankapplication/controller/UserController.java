package bankapplication.controller;

import bankapplication.api.AuthenticationRequest;
import bankapplication.api.AuthenticationResponse;
import bankapplication.api.RegisterRequest;
import bankapplication.api.UserInfoResponse;
import bankapplication.domain.Balance;
import bankapplication.domain.Role;
import bankapplication.domain.User;
import bankapplication.service.AuthenticationService;
import bankapplication.service.BalanceService;
import bankapplication.service.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.nio.file.attribute.UserPrincipal;
import java.util.List;

//@CrossOrigin(origins = "*", allowedHeaders = "Access-Control-Allow-Origin")
@RestController @RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final AuthenticationService authService;
    private final BalanceService balanceService;
    @GetMapping("/api/user-info")
    public ResponseEntity<UserInfoResponse> userinfo() {
        Authentication authenticationToken = SecurityContextHolder.getContext().getAuthentication();
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        User user = (User) authenticationToken.getPrincipal();
        userInfoResponse.setUser(user);
        Balance balance = balanceService.findBalanceByUser(user);
        userInfoResponse.setBalance(balance);
        return ResponseEntity.ok(userInfoResponse);
    }
    @GetMapping("/api/test")
    public ResponseEntity<String> sayhello() {
        return ResponseEntity.ok("hello");
    }
    @GetMapping("/api/users")
    public ResponseEntity<List<User>> getUsers(@AuthenticationPrincipal UserPrincipal principal) {
        Authentication authenticationToken = SecurityContextHolder.getContext().getAuthentication();

        return ResponseEntity.ok().body(

                userService.getUsers()
        );
    }
//    @PostMapping("/auth/register")
//    public ResponseEntity<AuthenticationResponse> register(
//        @RequestBody RegisterRequest request
//    ){
//        return ResponseEntity.ok(authService.register(request));
//    }

    @PostMapping("/auth/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(
            @RequestBody AuthenticationRequest request
    ){
        return ResponseEntity.ok(AuthenticationResponse.builder().token(authService.authenticate(request)).build());
    }

    @PostMapping("/api/user")
    public ResponseEntity<User>saveUser(@RequestBody User user) {
        return ResponseEntity.created(null).body(userService.saveUser(user));
    }

    @PostMapping("/" +
            "api/role")
    public ResponseEntity<Role>saveRole(@RequestBody Role role) {
        return ResponseEntity.ok().body(userService.saveRole(role));
    }

    @PostMapping("/role/addtouser")
    public ResponseEntity<?>addRoleToUser(@RequestBody RoleToUserForm form) {
        userService.addRoleToUser(form.getUsername(), form.getRolename());
        return  ResponseEntity.ok().build();
    }

    @Data
    class RoleToUserForm{
        private String username;
        private String rolename;
    }
}
