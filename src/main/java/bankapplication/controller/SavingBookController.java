package bankapplication.controller;

import bankapplication.api.CreateSavingBookRequest;
import bankapplication.api.WithDrawMoneySavingBookRequest;
import bankapplication.domain.Balance;
import bankapplication.domain.InterestRate;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.enumType.InterestPaymentType;
import bankapplication.exception.FailedCreateSavingBook;
import bankapplication.exception.FailedWithDraw;
import bankapplication.repo.*;
import bankapplication.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

//@CrossOrigin(origins = "*", allowedHeaders = "Access-Control-Allow-Origin")
@RestController
@RequiredArgsConstructor
public class SavingBookController {
	private final BankService bankService;
	private final UserRepo userRepo;
	private final SavingBookRepo savingBookRepo;
	private final RoleRepo roleRepo;
	private final BalanceRepo balanceRepo;

	private final InterestRateRepo interestRateRepo;
	@GetMapping("/api/savingbook")
	public ResponseEntity<List<SavingBook>> getAllSavingBook() {
		Authentication authenticationToken = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)authenticationToken.getPrincipal();
		List<SavingBook> savingBooks = bankService.getAllSavingBook(user);
		savingBooks.forEach(savingBook -> {

		});
		return ResponseEntity.ok().body(
				savingBooks
		);
	}
	@GetMapping("/api/savingbook/{id}")
	public ResponseEntity<List<SavingBook>> getSavingBookById(@PathVariable Long id) {
		Authentication authenticationToken = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)authenticationToken.getPrincipal();
		return ResponseEntity.ok().body(
				bankService.getSavingBook(id, user)
		);
	}

//


}
