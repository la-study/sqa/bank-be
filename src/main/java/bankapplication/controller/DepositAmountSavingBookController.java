package bankapplication.controller;

import bankapplication.api.WithDrawMoneySavingBookRequest;
import bankapplication.domain.Balance;
import bankapplication.domain.SavingBook;
import bankapplication.domain.User;
import bankapplication.exception.FailedWithDraw;
import bankapplication.repo.*;
import bankapplication.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Optional;
@RestController
@RequiredArgsConstructor
public class DepositAmountSavingBookController {
	private final BankService bankService;
	private final UserRepo userRepo;
	private final SavingBookRepo savingBookRepo;
	private final RoleRepo roleRepo;
	private final BalanceRepo balanceRepo;

	private final InterestRateRepo interestRateRepo;
	@PostMapping("/api/savingbook/width-draw")
	public ResponseEntity<SavingBook> depositSavingBook(@RequestBody WithDrawMoneySavingBookRequest withDrawMoneySavingBookRequest, @AuthenticationPrincipal User user) {
		Long savingBookId = withDrawMoneySavingBookRequest.getId();
		Optional<SavingBook> savingBookOptional = savingBookRepo.findById(savingBookId);
		if (savingBookOptional.isPresent() == false) throw new FailedWithDraw("Failed With Draw");
		SavingBook savingBook = savingBookOptional.get();
		if (savingBook.getUser().getId().compareTo(user.getId()) != 0) throw new FailedWithDraw("Failed With Draw");
		if (savingBook.getDepositAmount().compareTo(withDrawMoneySavingBookRequest.getDepositAmount()) == -1)
			throw new FailedWithDraw("Failed With Draw");
		savingBook.setDepositAmount(savingBook.getDepositAmount().subtract(withDrawMoneySavingBookRequest.getDepositAmount()));
		savingBookRepo.save(savingBook);
		Balance balance = balanceRepo.findByUser_id(user.getId());
		balance.setBalance(balance.getBalance().add(withDrawMoneySavingBookRequest.getDepositAmount()));
		balanceRepo.save(balance);
		return ResponseEntity.ok().body(
				savingBook
		);
	}
}
