package bankapplication.exception;

public class FailedAuthenticated extends RuntimeException {
	public FailedAuthenticated(String message) {
		super(message);
	}

}
