package bankapplication.exception;

public class FailedCreateSavingBook extends RuntimeException {
	public FailedCreateSavingBook(String message) {
		super(message);
	}

}
