package bankapplication.exception;

public class FailedRegister extends RuntimeException {
	public FailedRegister(String message) {
		super(message);
	}
}
