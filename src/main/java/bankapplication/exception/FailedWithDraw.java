package bankapplication.exception;

public class FailedWithDraw extends RuntimeException {
	public FailedWithDraw(String message) {
		super(message);
	}
}
