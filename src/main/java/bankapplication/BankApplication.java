package bankapplication;

import bankapplication.domain.InterestRate;
import bankapplication.domain.Role;
import bankapplication.domain.User;
import bankapplication.service.BankService;
import bankapplication.service.InterestRateService;
import bankapplication.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.math.BigDecimal;
import java.util.ArrayList;

@EnableScheduling
@SpringBootApplication
@CrossOrigin
public class BankApplication {
    public static void main(String[] args) {

        SpringApplication.run(BankApplication.class, args);
    }
    @Bean
    CommandLineRunner run(UserService userService, BankService bankService, InterestRateService interestRateService) {
        return args -> {
            interestRateService.saveInterestRate(new InterestRate(null, 5.0 ,3.0));
            interestRateService.saveInterestRate(new InterestRate(null, 6.0 ,4.0));
            interestRateService.saveInterestRate(new InterestRate(null, 6.5 ,6.0));
            interestRateService.saveInterestRate(new InterestRate(null, 7.0 ,12.0));
            interestRateService.saveInterestRate(new InterestRate(null, 1.0 ,0.0));

            userService.saveRole(new Role(null, "ROLE_USER"));
            userService.saveRole(new Role(null, "ROLE_MANAGER"));
            userService.saveRole(new Role(null, "ROLE_ADMIN"));
            userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

            userService.saveUser(
                    new User(
                            null,
                            "Dương Văn Giang",
                            "justmegiang@gmail.com",
                            "123qweA@",
                            "0867598835",
                            "justmegiang@gmail.com",
                     new ArrayList<>())
            );
            userService.saveUser(new User(null, "Giang Văn Dương", "justmegiang1@gmail.com",
                    "123qweA@","0867598835", "justmegiang@gmail.com",
                    new ArrayList<>()));

            userService.addRoleToUser("justmegiang@gmail.com", "ROLE_USER");
            userService.addRoleToUser("justmegiang1@gmail.com", "ROLE_USER");

            User join = userService.getUser("justmegiang@gmail.com");
            User mar = userService.getUser("justmegiang1@gmail.com");
            bankService.deposit(join, new BigDecimal("100000000"));
            bankService.deposit(mar, new BigDecimal("1000000000"));

        };
    }
}
