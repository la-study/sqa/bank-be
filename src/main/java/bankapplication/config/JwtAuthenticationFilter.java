package bankapplication.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter
		extends OncePerRequestFilter
		  {

	private final JwtService jwtService;
	@Autowired
	private UserDetailsService userDetailsService;
			  @Override
			  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
				  final String authHeader = request.getHeader("Authorization");
				  try {
					  if (authHeader == null || !authHeader.startsWith("Bearer ")) {
						  filterChain.doFilter(request, response);
						  return;
					  }

					  final String jwt = authHeader.substring(7);
					  final String username = jwtService.extractUsername(jwt);

					  if (username == null || SecurityContextHolder.getContext().getAuthentication() != null) {
						  filterChain.doFilter(request, response);
						  return;
					  }

					  try {
						  UserDetails userDetails = userDetailsService.loadUserByUsername(username);

						  if (jwtService.isTokenValid(jwt, userDetails)) {
							  UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
							  authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
							  SecurityContextHolder.getContext().setAuthentication(authToken);
						  }
					  } catch (AuthenticationException e) {
						  SecurityContextHolder.clearContext();
						  response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
						  return;
					  }
				  }catch (Exception exception) {
					  SecurityContextHolder.clearContext();
					  response.sendError(HttpStatus.UNAUTHORIZED.value(), exception.getMessage());
				  }

				  filterChain.doFilter(request, response);
			  }
}
